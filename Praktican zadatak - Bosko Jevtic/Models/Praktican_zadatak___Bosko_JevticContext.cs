﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Praktican_zadatak___Bosko_Jevtic.Models
{
    public class Praktican_zadatak___Bosko_JevticContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public Praktican_zadatak___Bosko_JevticContext() : base("name=Praktican_zadatak___Bosko_JevticContext")
        {
        }

        public System.Data.Entity.DbSet<Praktican_zadatak___Bosko_Jevtic.Models.Proizvod> Proizvods { get; set; }
    }
}
