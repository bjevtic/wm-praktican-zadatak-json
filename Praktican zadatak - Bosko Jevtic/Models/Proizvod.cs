﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Praktican_zadatak___Bosko_Jevtic.Models
{
    public class Proizvod
    {
        public int id { get; set; }
        public string naziv { get; set; }
        public string opis { get; set; }
        public string kategorija { get; set; }
        public string proizvodjac { get; set; }
        public string dobavljac { get; set; }
        public int cena { get; set; }
    }
}