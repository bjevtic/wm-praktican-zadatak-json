﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Praktican_zadatak___Bosko_Jevtic.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace Praktican_zadatak___Bosko_Jevtic.Controllers
{
    public class ProizvodsController : Controller
    {
        private Praktican_zadatak___Bosko_JevticContext db = new Praktican_zadatak___Bosko_JevticContext();
        
        // GET: Proizvods
        [HttpGet]
        public ActionResult Index()
        {

            // ocisti db
            db.Database.Delete();
            db.Database.Create();

            // ucitaj .json 
            string path = Server.MapPath("~/App_Data/") + "proizvodi.json";
            string json = System.IO.File.ReadAllText(path);
            
            // parse .json
            JArray jsonArray = JArray.Parse(json);
            foreach (var jObject in jsonArray)
            {
                if (jObject != null)
                {
                    Proizvod proiz = new Proizvod();

                    int x;
                    int.TryParse(jObject["id"].ToString(), out x);
                    proiz.id = x;

                    proiz.naziv = jObject["naziv"].ToString();
                    proiz.opis = jObject["opis"].ToString();
                    proiz.kategorija = jObject["kategorija"].ToString();
                    proiz.proizvodjac = jObject["proizvodjac"].ToString();
                    proiz.dobavljac = jObject["dobavljac"].ToString();

                    int.TryParse(jObject["cena"].ToString(), out x);
                    proiz.cena = x;

                    db.Proizvods.Add(proiz);
                }
                db.SaveChanges();
            }
            return View(db.Proizvods.ToList());
        }

        // GET: Proizvods/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvod proizvod = db.Proizvods.Find(id);
            if (proizvod == null)
            {
                return HttpNotFound();
            }
            return View(proizvod);
        }

        // GET: Proizvods/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Proizvods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,naziv,opis,kategorija,proizvodjac,dobavljac,cena")] Proizvod proizvod)
        {
            if (ModelState.IsValid)
            {
                // dodaj novi proizvod u db
                db.Proizvods.Add(proizvod);
                db.SaveChanges();

                db2json();
                return RedirectToAction("Index");
            }
            return View(proizvod);
        }

        // GET: Proizvods/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvod proizvod = db.Proizvods.Find(id);
            if (proizvod == null)
            {
                return HttpNotFound();
            }
            return View(proizvod);
        }

        // POST: Proizvods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,naziv,opis,kategorija,proizvodjac,dobavljac,cena")] Proizvod proizvod)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proizvod).State = EntityState.Modified;
                db.SaveChanges();

                db2json();
                return RedirectToAction("Index");
            }
            return View(proizvod);
        }

        // GET: Proizvods/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvod proizvod = db.Proizvods.Find(id);
            if (proizvod == null)
            {
                return HttpNotFound();
            }
            return View(proizvod);
        }

        // POST: Proizvods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proizvod proizvod = db.Proizvods.Find(id);
            db.Proizvods.Remove(proizvod);
            db.SaveChanges();

            db2json();
            return RedirectToAction("Index");
        }

        private void db2json()
        {
            var personlist = db.Proizvods.ToList();
            string jsondata = new JavaScriptSerializer().Serialize(personlist);
            string path = Server.MapPath("~/App_Data/");
            System.IO.File.WriteAllText(path + "proizvodi.json", jsondata);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
